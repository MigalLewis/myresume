import { Component, OnInit } from '@angular/core';
import { ExtendedProfile } from '../models/profile.model';
import { LoadingService } from '../service/loading.service';
import { AboutMeFirebaseService } from '../service/about-me-firebase.service';

@Component({
  selector: 'app-about-me-page',
  templateUrl: './about-me-page.component.html',
  styleUrls: ['./about-me-page.component.scss']
})
export class AboutMePageComponent implements OnInit {
  profile: ExtendedProfile;
  selectedTopic: string;
  loading: boolean;

  constructor(private aboutMeService: AboutMeFirebaseService,
              private loadingService: LoadingService) {
    this.selectedTopic = '';
    this.loadingService.isLoading().subscribe(data => {
      this.loading = data;
    });
  }

  ngOnInit(): void {
    this.loadingService.startSpinner();
    this.aboutMeService.getExtendedAboutMe().subscribe(data => {
      this.profile = data[0];
      this.loadingService.stopSpinner();
    });
  }

  changeTopic(topic: string) {
    this.selectedTopic = topic;
  }

}
