import { Component, OnInit } from '@angular/core';
import { Experience } from '../models/skill.model';
import { faCalendarAlt } from '@fortawesome/free-solid-svg-icons';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { faAngleUp } from '@fortawesome/free-solid-svg-icons';
import { LoadingService } from '../service/loading.service';
import { ExperienceFirebaseService } from '../service/experience-firebase.service';

@Component({
  selector: 'app-experience-page',
  templateUrl: './experience-page.component.html',
  styleUrls: ['./experience-page.component.scss']
})
export class ExperiencePageComponent implements OnInit {
  experienceList: Experience[];
  faCalendar = faCalendarAlt;
  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  loading: boolean;

  constructor(private experienceService: ExperienceFirebaseService,
              private loadingService: LoadingService) {
    this.loadingService.isLoading().subscribe(data => {
      this.loading = data;
    });
  }

  ngOnInit(): void {
    this.loadingService.startSpinner();
    this.experienceService.getCompanies().subscribe(data => {
      this.experienceList = data;
      this.loadingService.stopSpinner();
    });
  }

  getDateTo(timestamp) {
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];
    const oldDate: Date = new Date('1800-12-31');
    if (!(timestamp instanceof Date)) {
      const thisDate = new Date(timestamp.toDate());

      if (oldDate >= thisDate) {
        return 'Current';
      }
      return monthNames[thisDate.getMonth()] + '-' + thisDate.getFullYear();
    }
    return timestamp;
  }

  getDate(timestamp) {
    if (!(timestamp instanceof Date)) {
      return timestamp.toDate();
    }
    return timestamp;
  }
}
