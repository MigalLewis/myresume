import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { ExperiencePageComponent } from './experience-page/experience-page.component';
import { SkillsPageComponent } from './skills-page/skills-page.component';
import { AboutMePageComponent } from './about-me-page/about-me-page.component';
import { AddProfilePageComponent } from './admin/add-profile-page/add-profile-page.component';
import { LoginComponent } from './admin/login/login.component';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';
import { LandingComponent } from './landing/landing.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home/landing',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomePageComponent,
    children: [
      {
        path: 'landing',
        component: LandingComponent,
        data: {animation: 'HomePage'}
      },
      {
        path: 'experience',
        component: ExperiencePageComponent,
        data: {animation: 'ExperiencePage'}
      },
      {
        path: 'skills',
        component: SkillsPageComponent,
        data: {animation: 'SkillsPage'}
      },
      {
        path: 'aboutMe',
        component: AboutMePageComponent,
        data: {animation: 'AboutPage'}
      }
    ]
  },
  {
    path: 'admin/add/profile',
    component: AddProfilePageComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  },
  {
    path: 'login',
    component: LoginComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
