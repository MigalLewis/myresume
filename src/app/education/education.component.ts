import { Component, OnInit, Input } from '@angular/core';
import { Education } from '../models/education.model';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {
  @Input() educationList: Education[];

  constructor() { }

  ngOnInit(): void {
  }

}
