import { Component, OnInit, Input } from '@angular/core';
import { TimeLineItem } from '../models/timeLineItem.model';

@Component({
  selector: 'app-time-line-item',
  templateUrl: './time-line-item.component.html',
  styleUrls: ['./time-line-item.component.scss']
})
export class TimeLineItemComponent implements OnInit {
  @Input() timeLineItem: TimeLineItem;
  @Input() showDescription: boolean;

  constructor() {
    this.showDescription = false;
  }

  ngOnInit(): void {

  }

}
