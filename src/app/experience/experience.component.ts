import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Experience } from '../models/skill.model';
import { faBriefcase } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {
  @Input() experience: Experience;
  faBriefcase = faBriefcase;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }


}
