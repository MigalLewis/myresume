import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, OnChanges} from '@angular/core';
import { BubbleChartService } from '../service/bubble-chart.service';
import { Skill } from '../models/skill.model';
import { SkillsFirebaseService } from '../service/skills-firebase.service';

@Component({
  selector: 'app-bubble-chart',
  templateUrl: './bubble-chart.component.html',
  styleUrls: ['./bubble-chart.component.scss']
})
export class BubbleChartComponent implements OnInit, AfterViewInit, OnChanges{
  @Input() skills: Skill[];
  @ViewChild('chart') chartElement: ElementRef;
  options;
  selectedSkill: Skill;

  constructor(private d3PackedBubbleChartService: BubbleChartService,
              public skillService: SkillsFirebaseService) {


  }
  ngOnInit() {
    this.onHover();
  }

  ngOnChanges(){
    if (this.skills){
      this.d3PackedBubbleChartService.renderChart(this.options, this.skills);
    }
  }
  ngAfterViewInit() {
    this.options = {
      height: this.chartElement.nativeElement.offsetHeight,
      width: this.chartElement.nativeElement.offsetWidth
    };
  }

  onHover() {
    this.d3PackedBubbleChartService.getEventEmitter().subscribe(data =>
      this.selectedSkill = data
    );
  }

}
