import { Component, OnInit } from '@angular/core';
import { Experience } from '../models/skill.model';
import { ExperienceFirebaseService } from '../service/experience-firebase.service';

@Component({
  selector: 'app-time-line',
  templateUrl: './time-line.component.html',
  styleUrls: ['./time-line.component.scss']
})
export class TimeLineComponent implements OnInit {
  timeLineItems: Experience[];

  constructor(private experienceService: ExperienceFirebaseService) { }

  ngOnInit(): void {
    this.experienceService.getCompanies().subscribe(data => {
      this.timeLineItems = data;
    });
  }
}
