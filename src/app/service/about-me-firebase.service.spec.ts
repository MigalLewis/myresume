import { TestBed } from '@angular/core/testing';

import { AboutMeFirebaseService } from './about-me-firebase.service';

describe('AboutMeFirebaseService', () => {
  let service: AboutMeFirebaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AboutMeFirebaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
