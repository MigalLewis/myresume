import { Injectable } from '@angular/core';
import { FormField } from '../models/FormField.model';
import { FormArray, FormControl, Validators, FormGroup } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import 'firebase/storage';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormFieldControlService {

  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;


  constructor(private firestore: AngularFireStorage,
              private storage: AngularFireStorage) { }

  toFormGroup(questions: FormField<any>[]) {
    const group: any = {};

    questions.forEach(question => {

      if (question.iterable) {

        if (!Array.isArray(question.value)) {
          question.value = !!question.value ? [question.value] : [''];
        }

        const tmpArray: FormArray = question.required ? new FormArray([]) : new FormArray([], Validators.required);

        if (!question.value || !question.value.length) {
          tmpArray.push(new FormControl(''));
        } else {
          question.value.forEach(val => {
            tmpArray.push(new FormControl(val));
          });
        }

        group[question.key] = tmpArray;

      } else {

        group[question.key] = question.required ? new FormControl(question.value || '', Validators.required)
          : new FormControl(question.value || '');

      }

    });
    return new FormGroup(group);
  }

  uploadImage(file: any) {
    const filePath = '/profile/' + file.name;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.downloadURL = fileRef.getDownloadURL();
    this.uploadPercent = task.percentageChanges();
  }

}
