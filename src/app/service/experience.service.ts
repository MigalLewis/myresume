import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Experience } from '../models/skill.model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExperienceService {

  constructor(private httpClient: HttpClient) { }

  getCompanies(): Observable<Experience[]> {

    return this.httpClient.get<Experience[]>(environment.backend.url + '/about/me/experience');
  }

  getEmoticon(emoticon: string) {
    let emoticonImage = '';
    switch (emoticon) {
      case 'fun_challenge' : emoticonImage = '../../';
                             break;
      case 'easy' : emoticonImage = '../../';
                    break;
      case 'hard_work' : emoticonImage = '../../';
                         break;
      case 'ehh' : emoticonImage = '../../';
                   break;
      case 'fun' : emoticonImage = '../../';
                   break;
      default: emoticonImage = '../../images/experience/happy.png';
    }
  }

}
