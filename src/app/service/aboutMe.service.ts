import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile, ExtendedProfile } from '../models/profile.model';
import { HttpClient } from '@angular/common/http';
import { Education } from '../models/education.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AboutMeService {

  constructor( private httpClient: HttpClient) { }

  getAboutMe() {
    return this.httpClient.get<Profile>(environment.backend.url + '/about/me/profile/basic/' + 'Migal');
  }
  getExtendedAboutMe() {
    return this.httpClient.get<ExtendedProfile>(environment.backend.url + '/about/me/profile/extended/' + 'Migal');
  }

  getEducation(): Observable<Education[]> {
    return this.httpClient.get<Education[]>(environment.backend.url + '/about/me/education');
  }
}
