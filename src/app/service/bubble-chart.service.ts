import { Injectable, EventEmitter } from '@angular/core';
import * as d3 from 'd3';
import { Skill } from '../models/skill.model';
import { Selection, PieArcDatum } from 'd3';
import { Event } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BubbleChartService {
  node;
  skillHovered: EventEmitter<Skill>;

  constructor() {
    this.skillHovered = new EventEmitter();
  }


  createDataSet(data: Skill[]) {
    return {
      children: [
        {
          name: 'ABC',
          children: data
        }
      ]
    };
  }

  renderChart(options, dataset) {
    const height = options.height;
    const width = options.width;
    const color = d3.scaleOrdinal(d3.schemeCategory10);

    const simulation = d3.forceSimulation()
    .force('x', d3.forceX(width / 2).strength(0.05))
    .force('y', d3.forceY(height / 2).strength(0.05))
    .force('collide', d3.forceCollide(((d: any) => radiusScale(d.yearsExperience * d.score + 4))));

    const radiusScale = d3.scaleSqrt().domain([1, 100]).range([10, 80]);

    const svg = d3.select('#chart')
      .append('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('transform', 'translate(0,0');

    const defs = svg.append('defs');
    const imageDef = defs.selectAll('.skill-pattern')
    .data(dataset)
    .enter()
    .append('pattern')
    .attr('class', 'skill-pattern')
    .attr('id', ((d: Skill) => d.id))
    .attr('height', '100%')
    .attr('width', '100%')
    .attr('patternContentUnits', 'objectBoundingBox');

    imageDef.append('image')
    .attr('height', 1)
    .attr('width', 1)
    .attr('xmlns:xlink', 'http://www.w3.org/1999/xlink')
    .attr('xlink:href', (d: Skill) => d.image);

    simulation.nodes(dataset).on('tick', (() => {
      this.node.attr('transform', ( d => 'translate(' + d.x + ',' + d.y + ')') );
    }));

    this.node = svg.selectAll('.skill')
    .data(dataset)
    .enter()
    .append('g');

    const circle = this.node.append('circle')
      .attr('r', (d =>  radiusScale((d.yearsExperience * 0.5) * (d.score * 1.5))))
      .style('fill', ((d, i: any) => color(i)))
      .on('mouseover', ((d: Skill) => {
        this.skillHovered.emit(d);
    }));
    const image = this.node.append('circle')
      .attr('r', (d =>  radiusScale(d.yearsExperience * d.score) / 1.5))
      .attr('fill', ((d: Skill) => 'url(#' + d.id + ')'));
  }

  getEventEmitter(): EventEmitter<Skill> {
    return this.skillHovered;
  }


}
