import { TestBed } from '@angular/core/testing';

import { ExperienceFirebaseService } from './experience-firebase.service';

describe('ExperienceFirebaseService', () => {
  let service: ExperienceFirebaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExperienceFirebaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
