import { Injectable } from '@angular/core';
import { DropDownField, FormField, TextField, RangeField, ImageField, DateField, TableField } from '../models/FormField.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileFormService {


  constructor() { }

  getBasicFields(): FormField<any>[] {
    const FormFields: FormField<any>[] = [
      new TextField({
        key: 'firstName',
        label: 'First name',
        value: '',
        required: true,
        order: 1
      }),
      new TextField({
        key: 'lastName',
        label: 'Last name',
        value: '',
        required: true,
        order: 2
      }),
      new DateField({
        key: 'dateOfBirth',
        label: 'Date Of Birth',
        value: '',
        order: 3
      }),
      new TextField({
        key: 'occupation',
        label: 'Occupation',
        value: '',
        required: true,
        order: 4
      }),
      new ImageField({
        key: 'profileImage',
        label: 'Profile Image',
        order: 7
      }),

      new TextField({
        controlType: 'textarea',
        key: 'about',
        label: 'About Me Message',
        value: '',
        rows: 8,
        order: 8
      }),




      // new DropDownField({
      //   key: 'brave',
      //   label: 'Bravery Rating',
      //   options: [
      //     { key: 'solid', value: 'Solid' },
      //     { key: 'great', value: 'Great' },
      //     { key: 'good', value: 'Good' },
      //     { key: 'unproven', value: 'Unproven' }
      //   ],
      //   placeholder: 'Select one option',
      //   order: 3
      // }),

    ];

    return FormFields.sort((a, b) => a.order - b.order);
  }

  getContactFields(): FormField<any>[] {
    const FormFields: FormField<any>[] = [
      new TextField({
        key: 'email',
        label: 'Email',
        type: 'email',
        value: '',
        required: false,
        order: 1
      }),
      new TextField({
        key: 'phone',
        label: 'Phone Number',
        value: '',
        pattern: '(0)\d{9}/',
        required: false,
        order: 2
      }),

      new TextField({
        key: 'streetName',
        label: 'Street Name',
        value: '',
        required: false,
        order: 3
      }),
      new TextField({
        key: 'suburb',
        label: 'Suburb',
        value: '',
        required: false,
        order: 4
      }),
      new TextField({
        key: 'city',
        label: 'City',
        value: '',
        required: false,
        order: 5
      }),
      new TextField({
        key: 'province',
        label: 'Province',
        value: '',
        required: false,
        order: 6
      }),
      new TextField({
        key: 'country',
        label: 'Country',
        value: '',
        required: false,
        order: 7
      })
    ];

    return FormFields.sort((a, b) => a.order - b.order);
  }

  getSkillsFields(): FormField<any>[] {
    const FormFields: FormField<any>[] = [
      new TableField({
        key: 'skillsTable',
        label: 'Skills Table',
        fields: [
          new TextField({
            key: 'name',
            label: 'Name',
            value: '',
            required: true,
            order: 1
          }),
          new TextField({
            key: 'yearsExperience',
            label: 'Years Experience',
            value: '',
            required: false,
            order: 2
          }),
          new RangeField({
            key: 'level',
            label: 'Level',
            type: 'range',
            value: 6,
            min: 1,
            max: 10,
            order: 3,
            required: true,
          }),
          new ImageField({
            key: 'skillImage',
            label: 'Skill Image',
            order: 4
          })
        ]
      })
    ];

    return FormFields.sort((a, b) => a.order - b.order);
  }
}
