import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { Skill } from '../models/skill.model';
import { Node } from '../models/node';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private httpClient: HttpClient) { }

  getAllSkills(): Observable<Skill[]> {
    return this.httpClient.get<Skill[]>(environment.backend.url + '/about/me/skills/');
  }
  getType(score) {
    if (score < 5) {
      return 'danger';
    }
    else if (score >= 5 && score < 8 ) {
      return 'warning';
    }
    else {
      return 'success';
    }
  }
  getMax(): Observable<number> {
    return of(10);
  }

  getSkillMatrix(): Observable<Node[]> {
    const skillMatrix = [
      new Node(1, 'Java', 'blue', 400, 200, 60),
      new Node(1, 'Javascript', 'red', 300, 200, 40),
    ];
    return of(skillMatrix);
  }
}
