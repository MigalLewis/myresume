import { Injectable } from '@angular/core';
import { AngularFirestore, CollectionReference, DocumentReference } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Profile, ExtendedProfile } from '../models/profile.model';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AboutMeFirebaseService {
  DEFAULT_PROFILE = '../../assets/data/profile.json';
  DEFAULT_EXTENDED_PROFILE = '../../assets/data/extendedProfile.json';

  constructor(private firestore: AngularFirestore,
              private httpClient: HttpClient) {
  }

  getAboutMe(): Observable<Profile[]> {
    return this.firestore.collection<Profile>('profile').valueChanges();
  }
  getExtendedAboutMe() {
    return this.firestore.collection<ExtendedProfile>('extendedProfile').valueChanges();
  }


  addDefaultAboutMe() {
    this.httpClient.get<Profile>(this.DEFAULT_PROFILE).subscribe((data: Profile) => {
      data.dateOfBirth = new Date(1990, 7, 5);
      this.firestore.collection<Profile>('profile').add(data);
    });
  }

  addDefaultExtendedAboutMe() {
    this.httpClient.get<ExtendedProfile>(this.DEFAULT_EXTENDED_PROFILE).subscribe((data: ExtendedProfile) => {
      this.firestore.collection<ExtendedProfile>('extendedProfile').add(data);
    });
  }
}
