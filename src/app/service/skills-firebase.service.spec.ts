import { TestBed } from '@angular/core/testing';

import { SkillsFirebaseService } from './skills-firebase.service';

describe('SkillsFirebaseService', () => {
  let service: SkillsFirebaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SkillsFirebaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
