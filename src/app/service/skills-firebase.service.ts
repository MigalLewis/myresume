import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Skill } from '../models/skill.model';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SkillsFirebaseService {
  DEFAULT_SKILLS = '../../assets/data/skills.json';
  skillCount = 0;

  constructor(private firestore: AngularFirestore,
              private httpClient: HttpClient) { }

  getAllSkills(): Observable<Skill[]> {
    return this.firestore.collection<Skill>('skill').valueChanges();
  }
  getType(score) {
    if (score < 5) {
      return 'danger';
    }
    else if (score >= 5 && score < 8 ) {
      return 'warning';
    }
    else {
      return 'success';
    }
  }
  getMax(): Observable<number> {
    return of(10);
  }

  addDefaultSkills() {
    this.httpClient.get<Skill[]>(this.DEFAULT_SKILLS).subscribe((data: Skill[]) => {
      if (this.skillCount === 0) {
        data.forEach(skill => {
          this.firestore.collection<Skill>('skill').add(skill);
        });
        this.skillCount = 1;
      }
    });
  }
}
