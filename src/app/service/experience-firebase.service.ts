import { Injectable } from '@angular/core';
import { Experience } from '../models/skill.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ExperienceFirebaseService {
  DEFAULT_EXPERIENCE = '../../assets/data/experience.json';
  skillCount = 0;

  constructor(private firestore: AngularFirestore,
              private httpClient: HttpClient) { }

  getCompanies(): Observable<Experience[]> {
    return this.firestore.collection<Experience>('experience', ref =>
    ref.orderBy('dateFrom', 'asc')).valueChanges();
  }

  getEmoticon(emoticon: string) {
    let emoticonImage = '';
    switch (emoticon) {
      case 'fun_challenge' : emoticonImage = '../../';
                             break;
      case 'easy' : emoticonImage = '../../';
                    break;
      case 'hard_work' : emoticonImage = '../../';
                         break;
      case 'ehh' : emoticonImage = '../../';
                   break;
      case 'fun' : emoticonImage = '../../';
                   break;
      default: emoticonImage = '../../images/experience/happy.png';
    }
  }
  addDefaultExperience() {
    this.httpClient.get<Experience[]>(this.DEFAULT_EXPERIENCE).subscribe((data: Experience[]) => {
      if (this.skillCount === 0) {
        data.forEach(experience => {
          experience.dateFrom = new Date(experience.dateFrom);
          experience.dateTo = new Date(experience.dateTo);
          this.firestore.collection<Experience>('experience').add(experience);
        });
        this.skillCount = 1;
      }
    });
  }
}
