import { Component, OnInit, Input, TemplateRef, AfterViewInit } from '@angular/core';
import { Gallery } from '../models/gallery.model';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  photosGallery;

  constructor() { }

  ngOnInit(): void {
    this.photosGallery = 'https://photos.google.com/share/' +
    'AF1QipPgVwwcYVomC3ffupaBFPVxdaP5uvtco0aIp0xJLLh_g7eh60P9ZWHuQAxXrSzStg?key=R2YtdGhIRXFFaHAtTGVtZFdlQUhaUGRSeE9pa2ZR';
  }

}


