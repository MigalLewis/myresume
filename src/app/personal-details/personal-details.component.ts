import { Component, OnInit, Input } from '@angular/core';
import { Profile } from '../models/profile.model';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faAddressBook } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.scss']
})
export class PersonalDetailsComponent implements OnInit {
  @Input() profile: Profile;
  faPhone = faPhone;
  faEnvelope = faEnvelope;
  faAddressBook = faAddressBook;
  faLinkedin = faLinkedin;
  colorList: string[];
  selectedColors: string[];

  constructor(private router: Router) {
    this.selectedColors = [];
    this.colorList = [
      'rgb(214, 62, 137)',
      'rgb(20, 12, 54)',
      'rgb(97, 154, 199)',
      'rgb(103, 140, 44)',
      'rgb(166, 98, 207)',
      'rgb(197, 168, 4)',
      'rgb(235, 173, 92)'
    ];
    this.setRandomColors();
  }

  ngOnInit(): void {
  }
  goToAboutMe() {
    this.router.navigate(['home', 'aboutMe']);
  }
  goSkills() {
    this.router.navigate(['home', 'skills']);
  }
  goToExperience() {
    this.router.navigate(['home', 'experience']);
  }

  setRandomColors() {
    while (this.selectedColors.length <= 5) {
      const index = Math.floor((Math.random() * this.colorList.length) + 1);
      const color = this.colorList[index];
      if (!this.selectedColors.includes(color)){
        this.selectedColors.push(color);
      }
    }
  }
}
