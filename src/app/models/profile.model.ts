import { Pokemon } from './pokemon.model';
import { GoogleItem } from './googleItem.model';
import { Gallery } from './gallery.model';
import { Education } from './education.model';

export class Profile {
  id?: string;
  firstName: string;
  lastName: string;
  occupation: string;
  dateOfBirth: Date;
  emailAddress?: string;
  phoneNumber?: string;
  linkeden?: string;
  teamName: string;
  address?: Address;
  image: string;
  aboutMe: string;
  skills: string[];
  experience: string[];
  education?: Education;
}
export class Address {
  profileRef?: string;
  streetName: string;
  suburb: string;
  city: string;
  country: string;
}

export class ExtendedProfile extends Profile {
  message: string;
  favouritePokemon: Pokemon[];
  googleItems: GoogleItem[];
  hobbies: Gallery[];
}
