
export class GoogleItem {
  id?: number;
  name: string;
  image: string;
  extraInfo: string;
  rank: number;
  googleExamples: GoogleExample[];
}


export class GoogleExample {
  name: string;
  image: string;
  extraInfo: string;
}
