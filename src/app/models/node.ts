import { NgIf } from '@angular/common';

export class Node {
  // Optional - defining optional implementation properties - required for relevant typing assistance
  index?: number;
  skillName?: string;
  color?: string;

  x?: number;
  y?: number;
  vx?: number;
  vy?: number;
  fx?: number | null;
  fy?: number | null;
  radius?: number;
  group?: string;
  fontSize?: string;

  id: string;
  linkCount = 0;

  constructor(id, skillName?: string, color?: string, x?: number, y?: number, radius?: number, group?: string) {
    this.id = id;
    this.skillName = skillName;
    this.color = color;
    this.x = x;
    this.y = y;
    this.radius = radius;
    if (!this.radius) {
      this.radius = 10;
    }
    this.group = group;
    this.getFontClass();
  }


  getFontClass() {
    if ( this.radius > 40) {
      this.fontSize = 'large';
    } else {
      this.fontSize = 'small';
    }
  }
}
