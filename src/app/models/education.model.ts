
export class Education {
  id?: string;
  institute: string;
  certificationName: string;
  image: string;
  downloadLink?: string;
}


