
export class FormField<T> {
  value: T;
  key: string;
  label: string;
  required: boolean;
  order: number;
  controlType: string;
  placeholder: string;
  iterable: boolean;

  constructor(options: any) {
    this.value = options.value || '';
    this.key = options.key;
    this.label = options.label;
    this.required = options.required;
    this.order = options.order;
    this.placeholder = options.placeholder;
    this.controlType = options.controlType;
    this.iterable = options.iterable;
  }
}

export class DropDownField extends FormField<string> {
  controlType = 'dropdown';
  options: { key: string, value: string }[] = [];

  constructor(options: any) {
    super(options);
    this.options = options.options || [];
  }
}
export class TextField extends FormField<string> {
  controlType: string;
  type: string;
  min: number | string;
  max: number | string;
  placeholder: string;
  pattern: string;

  constructor(options: any) {
    super(options);
    this.controlType = options.controlType || 'input';
    this.type = options.type || 'text';
    this.min = options.min;
    this.max = options.max;
    this.placeholder = options.placeholder;
    this.pattern = options.pattern;
  }
}
export class RangeField extends TextField {
  controlType = 'range';
  type = 'range';

  constructor(options: any) {
    super(options);
  }
}
export class DateField extends TextField {
  controlType = 'date';
  type = 'text';
  config: string;

  constructor(options: any) {
    super(options);
    this.config = options.config || '{ dateInputFormat: \'YYYY-MM-DD\' }';
  }
}
export class ImageField extends FormField<string> {
  controlType = 'image';
  src: string;

  constructor(options: any) {
    super(options);
    this.src = options.src || '../../../assets/images/profile.png';
  }
}
export class TableField extends FormField<string> {
  controlType = 'table';
  fields: FormField<string>[];

  constructor(options: any) {
    super(options);
    this.fields = options.fields;
  }
}
