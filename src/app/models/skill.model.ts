
export class Skill {
  id?: string;
  name: string;
  score: number;
  yearsExperience: number;
  image: string;
}

export class Experience {
    id?: string;
    image: string;
    dateFrom: Date;
    dateTo: Date;
    company: string;
    position: string;
    description?: string;
    emoticon?: string;
    collapsed: boolean;
    projects: Project[];
}
export class Project {
  name: string;
  role?: string;
  shortDescription: string;
  details: string;
  technologies: string[];
}
