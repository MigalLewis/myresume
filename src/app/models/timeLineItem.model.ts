
export class TimeLineItem {
  id?: string;
  image: string;
  dateFrom: Date;
  dateTo: Date;
  company: string;
  position: string;
  description?: string;
}


