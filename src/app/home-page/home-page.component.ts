import { Component, OnInit } from '@angular/core';
import { Profile } from '../models/profile.model';
import { Skill } from '../models/skill.model';
import { RouterOutlet } from '@angular/router';
import { slideInAnimation } from '../service/animations.service';
import { LoadingService } from '../service/loading.service';
import { AboutMeFirebaseService } from '../service/about-me-firebase.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [slideInAnimation]
})
export class HomePageComponent implements OnInit {
  skills: Skill[];
  profile: Profile;
  loading: boolean;

  constructor(private aboutService: AboutMeFirebaseService,
              public loadingService: LoadingService) {
              }

  ngOnInit(): void {
    this.loadingService.isLoading().subscribe( data => {
      this.loading = data;
    });
    this.loadingService.startSpinner();
    this.aboutService.getAboutMe().subscribe(data => {
      this.profile = data[0];
      this.loadingService.stopSpinner();
    });
  }

  prepareRoute(outlet: RouterOutlet) {
    const key = 'animation';
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData[key];
  }
}
