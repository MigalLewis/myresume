import { Component, OnInit, Input, EventEmitter, OnChanges } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, AbstractControl } from '@angular/forms';
import { FormField } from '../../models/FormField.model';
import { Profile } from 'src/app/models/profile.model';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { FormFieldControlService } from 'src/app/service/form-field-control.service';


@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnInit, OnChanges {
  profile: Profile;
  imageUploadEventEmitter: EventEmitter<File>;
  faPlus = faPlus;
  tableData: any[][];

  @Input() formField: FormField<any>;
  @Input() form: FormGroup;
  tableForm: FormGroup;
  get isValid() { return this.form.controls[this.formField.key].valid; }

  constructor(private fb: FormBuilder,
              private formFieldControlService: FormFieldControlService) {
      this.tableData = [];

  }

  ngOnChanges() {
    if (this.formField.controlType === 'table') {
      this.tableForm = this.formFieldControlService.toFormGroup(this.formField[`fields`]);
    }
  }
  ngOnInit() {
  }

  public formFieldControl(): AbstractControl {
    return this.form.get(this.formField.key);
  }

  fileChangeEvent(fileInput, formKey) {
    if (fileInput.target.files && fileInput.target.files[0]) {
      (async () => {
        this.formFieldControlService.uploadImage(fileInput.target.files[0]);

        await this.delay(5000);

        this.formFieldControlService.uploadPercent.subscribe( data => {
          if (data === 100) {
            // this.snackbarService.add({
            //   msg: 'Upload complete!',
            //   background: '#5AB190',
            //   color: '#ffffff',
            //   timeout: 2000,
            //   action: {
            //     text: ''
            //   }
            // });
          }
        });
        this.formFieldControlService.downloadURL.subscribe( url => {
          this.tableForm.value[formKey] = url;
        });
        // Do something after

      })();


    }
  }
  private delay(ms: number)
  {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  addToTable() {
  }

}
