import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormField } from 'src/app/models/FormField.model';
import { FormGroup } from '@angular/forms';
import { FormFieldControlService } from 'src/app/service/form-field-control.service';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {
  @Input() title: string;
  @Input() formFields: FormField<any>[] = [];
  form: FormGroup;
  @Output() formEmitter: EventEmitter<any>;

  constructor(private qcs: FormFieldControlService) {
    this.formEmitter = new EventEmitter();
  }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.formFields);

  }

  onSubmit() {
    this.formEmitter.emit(this.form.value);
  }

}
