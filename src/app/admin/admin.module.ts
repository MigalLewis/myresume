import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddProfilePageComponent } from './add-profile-page/add-profile-page.component';
import { FormFieldComponent } from './form-field/form-field.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { LoginComponent } from './login/login.component';



@NgModule({
  declarations: [
    AddProfilePageComponent,
    FormFieldComponent,
    DynamicFormComponent,
    LoginComponent
  ],
  imports: [
    FontAwesomeModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    AngularFireStorageModule
  ],
  exports: [
    AddProfilePageComponent,
    DynamicFormComponent,
    LoginComponent
  ]
})
export class AdminModule { }
