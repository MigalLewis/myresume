import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public authService: AngularFireAuth) { }

  ngOnInit(): void {
  }

  loginWithGoogle() {
    this.authService.signInWithPopup(new auth.GoogleAuthProvider());
  }
  logout() {
    this.authService.signOut();
  }
}
