import { Component, OnInit } from '@angular/core';
import { ProfileFormService } from 'src/app/service/profile-form.service';
import { Profile } from 'src/app/models/profile.model';
import { EventEmitter } from 'protractor';
import { FormField } from 'src/app/models/FormField.model';

@Component({
  selector: 'app-add-profile-page',
  templateUrl: './add-profile-page.component.html',
  styleUrls: ['./add-profile-page.component.scss']
})
export class AddProfilePageComponent implements OnInit {
  profile: Profile;
  payLoad: any;
  stage: string;

  formFields: any[];
  contactFields: any[];
  skillsFields: FormField<any>[];

  constructor(service: ProfileFormService) {
    this.formFields = service.getBasicFields();
    this.contactFields = service.getContactFields();
    this.skillsFields = service.getSkillsFields();
    this.stage = 'skills';
  }

  ngOnInit(){

  }

  basicProfile(form: any) {
    this.payLoad = form;
    this.stage = 'contact';
  }

  contactDetails(form: any) {
    this.payLoad = form;
    this.stage = 'skills';
  }

  onAddSkills(form: any) {
    this.payLoad = form;
    this.stage = 'done';
  }
}
