import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PersonalDetailsComponent } from './personal-details/personal-details.component';
import { ExperienceComponent } from './experience/experience.component';
import { HomePageComponent } from './home-page/home-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TimeLineComponent } from './time-line/time-line.component';
import { TimeLineItemComponent } from './time-line-item/time-line-item.component';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { EducationComponent } from './education/education.component';
import { ExperiencePageComponent } from './experience-page/experience-page.component';
import { SkillsPageComponent } from './skills-page/skills-page.component';
import { BubbleChartComponent } from './bubble-chart/bubble-chart.component';
import { AboutMePageComponent } from './about-me-page/about-me-page.component';
import { GalleryComponent } from './gallery/gallery.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { GoogleComponent } from './google/google.component';
import { PokemonComponent } from './pokemon/pokemon.component';
import { BackgroundAnimationComponent } from './background-animation/background-animation.component';
import { AdminModule } from './admin/admin.module';
import { LandingComponent } from './landing/landing.component';
import { LoadingComponent } from './loading/loading.component';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';


@NgModule({
  declarations: [
    AppComponent,
    PersonalDetailsComponent,
    HomePageComponent,
    ExperienceComponent,
    TimeLineComponent,
    TimeLineItemComponent,
    EducationComponent,
    ExperiencePageComponent,
    SkillsPageComponent,
    BubbleChartComponent,
    AboutMePageComponent,
    GalleryComponent,
    GoogleComponent,
    PokemonComponent,
    BackgroundAnimationComponent,
    LandingComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ModalModule.forRoot(),
    HttpClientModule,
    ProgressbarModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,
    AngularFireAnalyticsModule,
    AdminModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
