import { Component, OnInit } from '@angular/core';
import { Skill } from '../models/skill.model';
import { LoadingService } from '../service/loading.service';
import { SkillsFirebaseService } from '../service/skills-firebase.service';

@Component({
  selector: 'app-skills-page',
  templateUrl: './skills-page.component.html',
  styleUrls: ['./skills-page.component.scss']
})
export class SkillsPageComponent implements OnInit {
  skills: Skill[];
  loading: boolean;

  constructor(public skillService: SkillsFirebaseService,
              private loadingService: LoadingService) {
  }

  ngOnInit(): void {
    this.loadingService.isLoading().subscribe(data => {
      this.loading = data;
    });
    this.loadingService.startSpinner();
    this.skillService.getAllSkills().subscribe(data => {
      this.skills = data;
      this.loadingService.stopSpinner();
    }, error => {
      console.log(error);
      this.loadingService.stopSpinner();
    });
  }
}
