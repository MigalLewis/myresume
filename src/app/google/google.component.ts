import { Component, OnInit, Input } from '@angular/core';
import { GoogleItem } from '../models/googleItem.model';

@Component({
  selector: 'app-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.scss']
})
export class GoogleComponent implements OnInit {
  googleImages: string[];
  @Input() googleItems: GoogleItem[];
  shouldShowMoreInfo: boolean;
  selectedGoogleItem: GoogleItem;

  constructor() {  }

  ngOnInit(): void {
    this.googleImages = [
      'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'
    ];
  }

  showMoreInfo(id: number) {
    this.shouldShowMoreInfo = true;
    this.selectedGoogleItem = this.googleItems.filter(g => g.id === id)[0];
  }

}
