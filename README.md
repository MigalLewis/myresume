# MyResume

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Font Awesome
https://bitbucket.org/omnipresentdev/omnipresent-web-lib/wiki/Font%20awesome


## Theme 
https://abouts.co/color/0c3f84


## Importing new Fonts
https://medium.com/@aditya_tyagi/import-fonts-in-an-angular-app-the-easy-right-way-ae9e99cab551



Images
Photo by Steve Johnson from Pexels
Photo by Steve Johnson from Pexels


https://tympanus.net/codrops/2013/08/27/3d-shading-with-box-shadows/

https://wallpaperaccess.com/digital-art


google quick draw


http://freeconnection.blogspot.com/2013/07/vectorize-image-with-python-scikit-image.html
https://www.fiverr.com/search/gigs?query=digital%20art&source=main_banner&search_in=everywhere&search-autocomplete-original-term=digital%20art&ref=main_type%3Alandscape%7Cpackage_includes%3Ain_colors
https://blogs.nvidia.com/blog/category/autonomous-machines/

### D3 bubble chart
https://www.youtube.com/watch?v=lPr60pexvEM


### Material Shadows
https://codepen.io/thir13n/pen/uEzme
